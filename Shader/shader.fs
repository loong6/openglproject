#version 330 core
out vec4 FragColor;

uniform vec4 vertexColor;
in vec3 ourColor;
in vec3 ourPos;

void main()
{
    FragColor = vec4(ourColor, vertexColor.g);
	//FragColor = vec4(ourPos,1.0);
}