#include "Shader.h"
#include <GLFW/glfw3.h>
#include "common.h"
#include <iostream>

using namespace std;

PRE_DEFINE_FUNC;


int main(int argc, char ** argv) {

	auto window = initWindow("ShaderTest", framebuffer_size_callback);

	Shader shaderProgram("shader.vs", "shader.fs");


	float vertices[] = {
		// 位置              // 颜色
	 0.5f, 0.5f, 0.0f,  0.9f, 0.0f, 0.0f,   // 右下
	-0.5f, 0.5f, 0.0f,  0.0f, 0.8f, 0.0f,   // 左下
	 0.0f,  -0.5f, 0.0f,  0.0f, 0.0f, 0.7f,    // 顶部

	 0.5f, 0.5f, 0.0f,  0.6f, 0.0f, 0.0f,    // 顶部
	  0.0f,  -0.5f, 0.0f,  0.0f, 0.5f, 0.0f,   // 顶部
	 1.0f,  -0.5f, 0.0f,  0.0f, 0.0f, 0.4f,    // 顶部

	 -0.5f, 0.5f, 0.0f,  0.3f, 0.0f, 0.0f,    // 顶部
	  0.0f,  -0.5f, 0.0f,  0.0f, 0.2f, 0.0f,   // 顶部
	 -1.0f,  -0.5f, 0.0f,  0.0f, 0.0f, 0.1f    // 顶部
	};

	unsigned int VBO, VAO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	// bind the Vertex Array Object first, then bind and set vertex buffer(s), and then configure vertex attributes(s).
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
	// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
	// glBindVertexArray(0);


	// bind the VAO (it was already bound, but just to demonstrate): seeing as we only have a single VAO we can 
	// just bind it beforehand before rendering the respective triangle; this is another approach.
	

	

	// render loop
	// -----------
	while (!glfwWindowShouldClose(window))
	{
		// input
		// -----
		process_input(window);

		// render
		// ------
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		// be sure to activate the shader before any calls to glUniform
		
		glUseProgram(shaderProgram.ID);
		// update shader uniform
		float timeValue = glfwGetTime();
		float greenValue = sin(timeValue) / 2.0f + 0.5f;
		
		shaderProgram.setVec4f("vertexColor", 0.0f, greenValue, 0.0f, 1.0f);
		shaderProgram.setVec2f("offset",greenValue / 2, -greenValue / 2);

		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 9);

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// optional: de-allocate all resources once they've outlived their purpose:
	// ------------------------------------------------------------------------
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);

	// glfw: terminate, clearing all previously allocated GLFW resources.
	// ------------------------------------------------------------------
	glfwTerminate();

	return 0;
}