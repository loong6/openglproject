#include "Shader.h"
#include "common.h"
#include <GLFW/glfw3.h>
#include "stb_image.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


PRE_DEFINE_FUNC;

using namespace std;


void process_input2(GLFWwindow *window) {
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {

	}
}


int main(int argc, char **argv) {

	auto window = initWindow("CoordinateSystem Test", framebuffer_size_callback);

	Shader shader("shader.vs", "shader.fs");




	while (!glfwWindowShouldClose(window)) {
		process_input(window);
		process_input2(window);

		glClearColor(0.2f, 0.3f, 0.4f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);



		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}