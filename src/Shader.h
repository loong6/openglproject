#ifndef SHADER_H_
#define SHADER_H_

#include <glad/glad.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

class Shader
{
public:
	Shader(const GLchar *vertexPath,const GLchar *fragPath);
	~Shader();
	
	void use();
	void setBool(const std::string &name, bool value) const;
	void setInt(const std::string &name, int value) const;
	void setFloat(const std::string &name, float value) const;
	void setVec2f(const std::string &name, float, float) const;
	void setVec4f(const std::string &name, float, float,float,float) const;
	unsigned int ID;


private:

};


#endif // !SHADER_H_
