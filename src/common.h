#ifndef COMMON_H_
#define COMMON_H_

#define PRE_DEFINE_FUNC \
GLFWwindow *initWindow(const char* title, GLFWframebuffersizefun); \
void framebuffer_size_callback(GLFWwindow *window, int width, int height); \
void process_input(GLFWwindow *window);\

#endif // !COMMON_H_

